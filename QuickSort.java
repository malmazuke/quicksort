public class QuickSort{
	public static void sort(int[] values){
		quickSort(values, 0, values.length-1);
	}

	private static void quickSort(int[] values, int left, int right){
		int pivot = partition(values, left, right);

		if (left < pivot-1)
			quickSort(values, left, pivot-1);
		if (pivot < right)
			quickSort(values, pivot, right);
	}

	private static int partition(int[] values, int left, int right){
		int pivot = values[(left+right)/2];
		int i = left, j = right;

		while (i <= j){
			while (values[i] < pivot)
				i++;
			while (values[j] > pivot)
				j--;
			if (i <= j){
				swap(values, i, j);
				i++;
				j--;
			}
		}

		return i;
	}

	private static void swap(int[] values, int left, int right){
		int temp = values[left];
		values[left] = values[right];
		values[right] = temp;
	}
}

